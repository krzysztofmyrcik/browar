export type Brewer = {
    product_id: number;
    name: string;
    size: string;
    price: string;
    beer_id: number;
    image_url: string;
    category: string;
    abv: string;
    style: string;
    attributes: string;
    type: string;
    brewer: string;
    country: string;
    on_sale: boolean;
}

export type FilteredBrewer = {
    name: string
    price: string
    type: string
    image_url: string
}
