export module Enums {

    export enum LocalStorageKeys {
        Brewers = 'Brewers',
        ListToLoad = 'ListToLoad',
        ItemsToLoad = 'ItemsToLoad'
    }
}