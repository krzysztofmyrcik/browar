import { OnInit, EventEmitter, Input, Output, Component } from '@angular/core';

@Component({
  selector: 'app-table-filter',
  templateUrl: './table-filter.component.html',
  styleUrls: ['./table-filter.component.css']
})
export class TableFilterComponent implements OnInit {

  private downArrow: string = '˅'
  private upArrow: string = '˄'
  private disabledSign: string = 'x'
  public chosenSign: string
  @Input() isDisabled: boolean
  @Output() getArrayData = new EventEmitter<any>();
  constructor() { }

  ngOnChange() {
  }

  ngOnInit() {
    this.setButtonSign()
  }

  private setButtonSign() {
    this.chosenSign = this.isDisabled ? this.disabledSign : this.downArrow
  }

  private changeArrow(event: EventEmitter<any>): void {
    if (this.chosenSign === this.downArrow) {
      this.chosenSign = this.upArrow
    } else if (this.chosenSign === this.upArrow) {
      this.chosenSign = this.downArrow
    }
    console.log(event)
    this.getArrayData.emit(event)
  }

}
