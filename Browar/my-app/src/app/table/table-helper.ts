export class TableHelper {
    private static columnName: string = 'column'
    private static id: number = 0
    public static getColumnId(){
        this.id++
        return this.columnName + this.id.toString()
    }
}