import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.css']
})
export class TooltipComponent implements OnInit {

  @Input() source: string
  @Output() hide: EventEmitter<boolean> = new EventEmitter();

  private hideElement: boolean
  private previousElement: string
  constructor() {
    this.hideElement = false
  }

  ngOnInit() {
  }
  ngOnChanges() {
    if (this.source === this.previousElement) {
      this.hideElement = true
    } else {

      this.hideElement = false
    }
    this.previousElement = this.source

  }

  public hidePopup() {
    this.hideElement = true
    this.hide.emit(true)
  }
}
