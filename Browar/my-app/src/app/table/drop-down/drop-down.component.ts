import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';

@Component({
  selector: 'app-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.css']
})
export class DropDownComponent implements OnInit {
  private title: string
  @Input() dropList: string[]
  @Output() emitBrewerName: EventEmitter<any> = new EventEmitter()


  constructor() { }

  ngOnInit() {}

  private setBrewerName(value: any): void{
    this.title = value
    this.emitBrewerName.emit(value)
  }
}
