import * as _ from 'lodash'

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Brewer, FilteredBrewer } from 'src/models/brewer';
import { LocalStorageService } from '../local-storage/local-storage.service';
import { TableHelper } from './table-helper';
import { Enums } from 'src/models/enums';
import { BrewerService } from '../services/brewer-service.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @Output() columnButtonEvent: EventEmitter<string> = new EventEmitter<string>()
  @Input() brewers: Brewer[]
  @Input() itemsToLoad: number

  public disableFilter: boolean = true

  public get RESTRICTED_LENGTH(): number { return this._restrictedLength }
  public set RESTRICTED_LENGTH(value: number) { this._restrictedLength = value }
  private _restrictedLength: number

  private columnName: string
  private selectedOption: string
  public tableData: FilteredBrewer[]
  public brewerNames: string[]
  public isLoadMoreButtonVisible: boolean
  private reamaningBrewers: FilteredBrewer[]
  public selectedBrewer: string = ''
  public imageSource: string

  constructor(private localStorage: LocalStorageService, private brewerService: BrewerService) {
    this.columnName = TableHelper.getColumnId()
  }

  ngOnChanges() {
    this.RESTRICTED_LENGTH = this.itemsToLoad
  }

  ngOnInit() {
    this.getBeersFromLocalStorage()
  }

  private getBeersFromLocalStorage(): void {
    const selectedBrewerFromLocalStorage: string = this.localStorage.getFromLocalStorage(this.columnName)
    this.selectedBrewer = selectedBrewerFromLocalStorage
    if (_.size(selectedBrewerFromLocalStorage) > 0) {
      console.info('get brewer for ' + this.columnName + ' from localstorage')
      const savedTableDataLength: number = parseInt(this.localStorage.getFromLocalStorage(this.columnName + Enums.LocalStorageKeys.ListToLoad))
      const tempBrewer: FilteredBrewer[] = this.filterBrewers(this.brewers, selectedBrewerFromLocalStorage)

      this.tableData = tempBrewer.splice(0, savedTableDataLength)
      this.reamaningBrewers = tempBrewer
      this.toggleLoadButton(_.size(tempBrewer) > 0)
    }
    this.getBrewerNames()
  }

  private getBeersLength(arr: any[]): boolean {
    return _.size(arr) > this.RESTRICTED_LENGTH ? true : false
  }

  private getBrewerNames(): void {
    const tempArr: string[] = _.uniq(this.brewers.map((e: Brewer) => {
      return e.brewer
    }))
    this.brewerNames = tempArr.sort()
  }

  public getSelectedBrewaryBeers(selectedBrewer: string): void {
    console.log(selectedBrewer + 'eloeloelo')
    this.localStorage.saveToLocalStorage({ key: this.columnName, value: selectedBrewer })
    const tempBrewers: FilteredBrewer[] = this.filterBrewers(this.brewers, selectedBrewer)
    this.handleSelectedBoxClicked(tempBrewers)
  }

  private handleSelectedBoxClicked(tempBrewers: FilteredBrewer[]): void {
    if (this.getBeersLength(tempBrewers)) {
      this.tableData = tempBrewers.splice(0, this.RESTRICTED_LENGTH)
      this.reamaningBrewers = tempBrewers
    } else {
      this.tableData = tempBrewers
    }

    this.toggleLoadButton(_.size(this.reamaningBrewers) > 0)
    this.localStorage.saveToLocalStorage({ key: this.columnName + Enums.LocalStorageKeys.ListToLoad, value: this.tableData.length })
  }

  private filterBrewers(tempBrewers: Brewer[], selectedBrewer: any): FilteredBrewer[] {
    return tempBrewers.filter((brewer: Brewer) => brewer.brewer === selectedBrewer)
      .map((e: Brewer) => {
        return { name: e.name, price: e.price, type: e.type, image_url: e.image_url }
      });
  }

  public getNext15Elements(): void {
    console.log(this.RESTRICTED_LENGTH)
    if (_.size(this.reamaningBrewers) >= this.RESTRICTED_LENGTH) {
      this.tableData = [].concat(...this.tableData, ...this.reamaningBrewers.splice(0, this.RESTRICTED_LENGTH))
    } else if (_.size(this.reamaningBrewers) > 0) {
      this.tableData = [].concat(...this.tableData, ...this.reamaningBrewers.splice(0, this.reamaningBrewers.length))
    }
    this.localStorage.saveToLocalStorage({ key: this.columnName + Enums.LocalStorageKeys.ListToLoad, value: this.tableData.length })
    this.toggleLoadButton(_.size(this.reamaningBrewers) > 0)
  }

  private toggleLoadButton(toggle: boolean): void {
    this.isLoadMoreButtonVisible = toggle
  }

  public filterTableByRow(event: any) {
    if (event === '˅') {
      this.tableData.sort((a, b) => (a.name > b.name) ? 1 : -1)
    } else {
      this.tableData.reverse()
    }
  }

  public open(imageSrc: string): void {
    this.imageSource = imageSrc
  }

  private clearImageSource(event: boolean): void {
    this.imageSource = ''
  }

  private onRowDoubleClick(sth) {
    console.log('sth')
    console.log(this.brewerService.getBeerById(1))
  }
}
