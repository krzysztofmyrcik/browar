import { Component, Output, EventEmitter } from '@angular/core'
import { BrewerService } from '../services/brewer-service.service'
import { Brewer } from 'src/models/brewer'
import * as _ from 'lodash'
import { LocalStorageService } from '../local-storage/local-storage.service'
import { Enums } from 'src/models/enums'

@Component({
  selector: 'app-brewer',
  templateUrl: './brewer.component.html',
  styleUrls: ['./brewer.component.css']
})
export class BrewerComponent {

  private title: string = 'my-app'
  public tables: number[] = [1, 2, 3]
  public loadedItems: number[] = [5, 10]
  public exportedBrewers: Brewer[]
  public howManyItemsShouldBeLoaded: number

  constructor(private appService: BrewerService, private localStorage: LocalStorageService) {
    this.getData()
  }

  private getAllBerewer() {
    const dataFromLocalStorage: any = this.localStorage.getFromLocalStorage(Enums.LocalStorageKeys.Brewers)
    if (_.size(dataFromLocalStorage) > 0) {
      console.info('getting data from localStorage')

      this.mapToBrewerObject(dataFromLocalStorage)
    } else {
      this.appService.getAllBeers().subscribe((response: Brewer[]) => {
        this.localStorage.saveToLocalStorage({ key: Enums.LocalStorageKeys.Brewers, value: response })
        this.mapToBrewerObject(response)
      })
    }
  }

  private mapToBrewerObject(arr: Brewer[]): void {
    this.exportedBrewers = arr.sort((a: Brewer, b: Brewer) => (a.name < b.name) ? 1 : -1).map((brewer: Brewer) => {
      return brewer
    })
  }

  private getData() {
    this.getItemsLenghtToLoad()
    this.getAllBerewer()
  }

  public setItemsLengthToLoad(value: string) {
    this.howManyItemsShouldBeLoaded = parseInt(value)
    this.localStorage.saveToLocalStorage({ key: Enums.LocalStorageKeys.ItemsToLoad, value: value })

  }

  private getItemsLenghtToLoad(value: number = this.loadedItems[0]) {
    this.howManyItemsShouldBeLoaded = parseInt(this.localStorage.getFromLocalStorage(Enums.LocalStorageKeys.ItemsToLoad))
    if (this.howManyItemsShouldBeLoaded == null || _.isNaN(this.howManyItemsShouldBeLoaded)) {
      this.howManyItemsShouldBeLoaded = value
      this.setItemsLengthToLoad(value.toString())
    }
  }

}
