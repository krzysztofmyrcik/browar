import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Brewer } from 'src/models/brewer';
import { BrewerDetailComponent } from '../brewer/brewer-detail/brewer-detail.component';

export interface Config {
    brewerUrl: string
}

@Injectable()
export class BrewerService implements Config {
    public brewerUrl: string;
    private httpOptions: any
    
    constructor(private httpClient: HttpClient) {
        this.brewerUrl = "http://ontariobeerapi.ca/beers/",
            
        this.httpOptions = {
            headers: new HttpHeaders({ 'Access-Control-Allow-Origin': '*' })
        };
    }
    public getAllBeers() {
        return this.httpClient.get<Brewer[]>(this.brewerUrl)
    }
    public getBeerById(id: number) {
        return this.httpClient.get<Brewer>(this.brewerUrl+id.toString())
        .subscribe((brewer: Brewer) => {
            if(brewer != null) {
                return brewer
            }
            return null
        })
        
    }
}


