/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing'
import { BrewerService } from './brewer-service.service'
import { HttpClient } from '@angular/common/http';

describe('Service: BrewerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BrewerService]
    });
  });

  it('should ...', inject([BrewerService], (service: BrewerService) => {
    expect(service).toBeTruthy();
  }));

  it('should get data',inject([BrewerService], (service: BrewerService) => {
    expect(service.getAllBeers()).toBeTruthy()
  }))

  it('should get single brewer', inject([BrewerService], (service: BrewerService) => {
    expect(service.getBeerById(1)).toBeTruthy()
  }))
});
