import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrewerComponent } from './brewer/brewer.component';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { BrewerService } from './services/brewer-service.service';
import { TableComponent } from './table/table.component';
import { LocalStorageService } from './local-storage/local-storage.service';
import { TableFilterComponent } from './table/table-filter/table-filter.component';
import { TooltipComponent } from './table/tooltip/tooltip.component';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BrewerDetailComponent } from './brewer/brewer-detail/brewer-detail.component';
import { DropDownComponent } from './table/drop-down/drop-down.component';

@NgModule({
   declarations: [
      AppComponent,
      BrewerComponent,
      TableComponent,
      TableFilterComponent,
      TooltipComponent,
      HeaderComponent,
      BrewerDetailComponent,
      DropDownComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule
   ],
   providers: [
      BrewerService,
      LocalStorageService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
