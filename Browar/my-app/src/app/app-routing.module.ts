import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrewerComponent } from './brewer/brewer.component';
import { BrewerDetailComponent } from './brewer/brewer-detail/brewer-detail.component';


const routes: Routes = [
  { path: 'brewery', component: BrewerComponent },
  { path: 'brewery/:id', component: BrewerDetailComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
