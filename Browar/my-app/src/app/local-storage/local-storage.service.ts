import { Injectable } from '@angular/core';
import { KeyValuePair } from 'src/models/interfaces';
import * as _ from "lodash"
import { Enums } from 'src/models/enums';

@Injectable({
  providedIn: 'root'
})

export class LocalStorageService {

  constructor() { }

  public dataToSave(data: KeyValuePair[]) {
    if(_.size(data) > 0) {
      data.forEach(element => {
        
        this.saveToLocalStorage(element)
      });
    }

  }

  public saveToLocalStorage(keyVlaue: KeyValuePair) {
    window.localStorage.setItem(keyVlaue.key, JSON.stringify(keyVlaue.value))
  }

  public getFromLocalStorage(key: Enums.LocalStorageKeys| string): string {
    return JSON.parse(window.localStorage.getItem(key))
  }

}
