import { KeyValuePair } from 'src/models/interfaces'

export class LocalStorageHelper {
    public static convertToKeyValue(data: any): KeyValuePair[]{
        const keyValues: KeyValuePair[] = []
        data.forEach((element: any) => {
        for(let key in element){
            const obj: KeyValuePair= {
                key: key,
                value: element[key]
            }
            keyValues.push(obj)
        }
            
        })
        return keyValues
    }
}